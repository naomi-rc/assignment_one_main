## Purpose
This application is an introduction to Android activities, dialog box, progress bar, alert dialog, Toasts and Log messages

## Problem
The application shouls ba able to calculate the distance between a pair of locations. The application prompts the user to enter two sets of coordinates (longitude and latitude).
Upon clicking the 'compute distnce' button, the app opens a dialog box that prompt the user to enter the two sets of coordinates and if the enetered values are valid, the distance is shown using an alert dialog box.
A progress bar is displayed after closing the prompt dialog box and showing the result.

![figure1](images/fig1.PNG)
![figure2](images/fig2.PNG)
![figure3](images/fig3.PNG)